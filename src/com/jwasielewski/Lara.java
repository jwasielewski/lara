package com.jwasielewski;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User:    Jędrzej Wasielewski
 * Date:    2/2/13
 * Time:    6:43 PM
 * License:    BSD
 */
public class Lara extends JFrame {

    private boolean isApplicationVisible;
    private boolean isClosingFromTray;
    private boolean isListUpdating;

    private SystemTray systemTray;
    private TrayIcon trayIcon;
    private PopupMenu trayPopupMenu;
    private MenuItem trayMenuExit;
    private CheckboxMenuItem trayMenuShowApp;

    private JPanel mainPanel;
    private JList widgetList;
    private JScrollPane scrollPane;
    private JButton addButton;
    private JButton updateButton;
    private JButton removeButton;
    private JButton clearButton;
    private JTextField textField;
    private JLabel[] labels;
    private JSpinner[] spinners;
    private JCheckBox autostart;
    private int selectedWidgetIndex;

    private LSettings settings;
    private Vector<String> widgetSettings;
    private DefaultListModel<String> widgetNamesList;
    private Vector<LWidget> widgets;

    public Lara(final String[] args) {
        setTitle("Lara");
        setSize(new Dimension(344, 430));
        setResizable(false);
        setLocationRelativeTo(null);

        isClosingFromTray = false;
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(isClosingFromTray) {
                    saveSettings();
                    System.exit(0);
                }
                else {
                    //do nothing
                    setVisible(false);
                    isApplicationVisible = false;
                    trayMenuShowApp.setState(false);
                }
            }
        });

        ImageIcon myAppImage = loadIcon("/img/Lara_icon.png");
        if(myAppImage != null) {
            setIconImage(myAppImage.getImage());
        }

        if(SystemTray.isSupported()) {
            systemTray = SystemTray.getSystemTray();

            trayPopupMenu = new PopupMenu();
            trayMenuShowApp = new CheckboxMenuItem("Show");
            trayMenuShowApp.setState(true);
            trayMenuShowApp.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent itemEvent) {
                    if(isApplicationVisible) {
                        setVisible(false);
                        isApplicationVisible = false;
                    }
                    else {
                        setVisible(true);
                        isApplicationVisible = true;
                    }
                }
            });
            trayPopupMenu.add(trayMenuShowApp);
            trayPopupMenu.addSeparator();

            trayMenuExit = new MenuItem("Close");
            trayMenuExit.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    isClosingFromTray = true;
                    saveSettings();
                    System.exit(0);
                }
            });
            trayPopupMenu.add(trayMenuExit);

            ImageIcon trayImageIcon = loadIcon("/img/Lara_icon_16.png");
            trayIcon = new TrayIcon(trayImageIcon.getImage(), "Lara");
            trayIcon.setPopupMenu(trayPopupMenu);
            try {
                systemTray.add(trayIcon);
            }
            catch (AWTException e) {
                System.out.println("TrayIcon could not be added.");
            }
        }

        if(args.length > 0 && args[0].equals("-q")) {
            setVisible(false);
            isApplicationVisible = false;
            trayMenuShowApp.setState(false);
        }
        else {
            setVisible(true);
            isApplicationVisible = true;
            trayMenuShowApp.setState(true);
        }

        //System.out.println(System.getProperty("java.class.path"));

        selectedWidgetIndex = -1;
        widgetNamesList = new DefaultListModel<String>();
        isListUpdating = false;

        settings = new LSettings();
        widgetSettings = new Vector<String>();
        widgets = new Vector<LWidget>();
        loadSettings();

        initializeComponents();
        updateWidgetList();
    }

    @SuppressWarnings("unchecked")
    private void initializeComponents() {
        mainPanel = new JPanel();
        mainPanel.setLayout(null);

        widgetList = new JList(widgetNamesList);
        //widgetList.setBounds(5, 5, 327, 120);
        //widgetList.setPreferredSize(new Dimension(320, 100));
        widgetList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        widgetList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting() && !isListUpdating) {
                    selectedWidgetIndex = widgetList.getSelectedIndex();
                    String[] s = widgets.elementAt(selectedWidgetIndex).getData().split("\\|");
                    String[] d = s[1].split(",");

                    textField.setText(s[0]);
                    spinners[0].setValue(Integer.valueOf(d[2]));
                    spinners[1].setValue(Integer.valueOf(d[1]));
                    spinners[2].setValue(Integer.valueOf(d[0]));
                    spinners[3].setValue(Integer.valueOf(d[3]));
                    spinners[4].setValue(Integer.valueOf(d[4]));
                }
            }
        });

        scrollPane = new JScrollPane();
        scrollPane.setBounds(5, 5, 327, 120);
        scrollPane.getViewport().add(widgetList, null);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        mainPanel.add(scrollPane);

        addButton = new JButton("Add");
        addButton.setBounds(5, 130, 160, 25);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selectedWidgetIndex == -1) {
                    isListUpdating = true;
                    String data = "";
                    data += textField.getText() + "|";
                    data += String.valueOf(spinners[2].getValue()) + ","
                            + String.valueOf(spinners[1].getValue()) + ","
                            + String.valueOf(spinners[0].getValue()) + ","
                            + String.valueOf(spinners[3].getValue()) + ","
                            + String.valueOf(spinners[4].getValue()) + "|";
                    data += "0,0";
                    LWidget tmp = new LWidget();
                    tmp.setData(data);
                    widgets.add(tmp);
                    widgets.lastElement().repaint();
                    widgetNamesList.addElement(textField.getText());
                    clear();
                    isListUpdating = false;
                }
            }
        });
        mainPanel.add(addButton);

        updateButton = new JButton("Update");
        updateButton.setBounds(5, 157, 160, 25);
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selectedWidgetIndex != -1) {
                    isListUpdating = true;
                    String[] p = widgets.elementAt(selectedWidgetIndex).getData().split("\\|")[2].split(",");
                    String data = "";
                    data += textField.getText() + "|";
                    data += String.valueOf(spinners[2].getValue()) + ","
                            + String.valueOf(spinners[1].getValue()) + ","
                            + String.valueOf(spinners[0].getValue()) + ","
                            + String.valueOf(spinners[3].getValue()) + ","
                            + String.valueOf(spinners[4].getValue()) + "|";
                    data += p[0] + "," + p[1];
                    LWidget tmp = new LWidget();
                    tmp.setData(data);
                    widgets.remove(selectedWidgetIndex);
                    widgets.add(tmp);
                    widgets.lastElement().repaint();
                    widgetNamesList.removeElementAt(selectedWidgetIndex);
                    widgetNamesList.addElement(textField.getText());
                    clear();
                    isListUpdating = false;
                }
            }
        });
        mainPanel.add(updateButton);

        removeButton = new JButton("Remove");
        removeButton.setBounds(172, 130, 160, 25);
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selectedWidgetIndex != -1) {
                    isListUpdating = true;
                    widgets.elementAt(selectedWidgetIndex).setVisible(false);
                    widgets.remove(selectedWidgetIndex);
                    widgetNamesList.removeElementAt(selectedWidgetIndex);
                    clear();
                    isListUpdating = false;
                }
            }
        });
        mainPanel.add(removeButton);

        clearButton = new JButton("Clear");
        clearButton.setBounds(172, 157, 160, 25);
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });
        mainPanel.add(clearButton);

        labels = new JLabel[6];
        spinners = new JSpinner[5];

        for(int i=0; i<labels.length; i++) {
            labels[i] = new JLabel();
            labels[i].setHorizontalAlignment(JLabel.CENTER);
            labels[i].setVerticalAlignment(JLabel.CENTER);
            labels[i].setBounds(5, 187+i*25+i*5, 160, 25);
        }
        labels[0].setText("Name");
        labels[1].setText("Day");
        labels[2].setText("Month");
        labels[3].setText("Year");
        labels[4].setText("Hour");
        labels[5].setText("Minutes");

        for(JLabel l : labels) {
            mainPanel.add(l);
        }

        textField = new JTextField();
        textField.setBounds(173, 187, 160, 25);
        mainPanel.add(textField);

        for(int i=0; i<spinners.length; i++) {
            spinners[i] = new JSpinner();
            spinners[i].setBounds(173, 217+i*25+i*5, 160, 25);
        }

        spinners[0].setModel(new SpinnerNumberModel(1,1,31,1));
        spinners[1].setModel(new SpinnerNumberModel(1,1,12,1));
        spinners[2].setModel(new SpinnerNumberModel(2013,2013,2200,1));
        spinners[3].setModel(new SpinnerNumberModel(0,0,23,1));
        spinners[4].setModel(new SpinnerNumberModel(0,0,59,1));

        for(JSpinner s : spinners) {
            mainPanel.add(s);
        }

        autostart = new JCheckBox("Add to autostart");
        autostart.setHorizontalAlignment(JCheckBox.CENTER);
        autostart.setVerticalAlignment(JCheckBox.CENTER);
        autostart.setBounds(5, 367, 327, 25);
        autostart.setEnabled(false);
        mainPanel.add(autostart);

        add(mainPanel);
    }

    private void clear() {
        selectedWidgetIndex = -1;
        widgetList.setSelectedIndex(selectedWidgetIndex);
        textField.setText("");
        spinners[0].setValue(1);
        spinners[1].setValue(1);
        spinners[2].setValue(2013);
        spinners[3].setValue(0);
        spinners[4].setValue(0);
    }

    @SuppressWarnings("unchecked")
    private void updateWidgetList() {
        widgetNamesList.clear();
        for(String s : widgetSettings) {
            widgetNamesList.addElement(s.split("\\|")[0]);
        }
        widgetList.setModel(widgetNamesList);
        widgetList.setSelectedIndex(selectedWidgetIndex);
    }

    private void saveSettings() {
        widgetSettings.clear();
        for(LWidget lw : widgets) {
            //System.out.println("[DEBUG] saveSettings: " + lw.getData());
            widgetSettings.add(lw.getData());
            lw.setVisible(false);
            lw.dispose();
        }
        try {
            settings.writeSettings(widgetSettings);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadSettings() {
        try {
            widgetSettings = settings.readSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(!widgetSettings.isEmpty()) {
            for(int i=0; i<widgetSettings.size(); i++) {
                widgets.add(new LWidget());
                widgets.lastElement().setData(widgetSettings.elementAt(i));
            }
        }
    }

    private ImageIcon loadIcon(String strPath)
    {
        URL img = getClass().getResource(strPath);
        if(img != null) {
            return new ImageIcon(img);
        }
        else {
            return null;
        }
    }

    public static void main(final String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (UnsupportedLookAndFeelException e) {
            System.out.println("1");
        }
        catch (ClassNotFoundException e) {
            System.out.println("2");
        }
        catch (InstantiationException e) {
            System.out.println("3");
        }
        catch (IllegalAccessException e) {
            System.out.println("4");
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Lara(args);
            }
        });
    }
}
